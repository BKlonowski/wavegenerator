#include "Sinus/SinusC.h"
#include "Triangle\Triangle.hpp"
#include "Rectangle\Rectangle.hpp"
#include "UserInterface\UIC.hpp"

#include <memory>


int main( int argc, char** argv )
{
    try
    {
        UICC Interface( argc, argv );

        if( Interface.IsHelpNeeded() )
            Interface.HandleHelpParameter();

        if( Interface.AreParametersAvailableAndCorrect() )
        {
            std::shared_ptr<WaveC> Wave;
            switch( Interface.GetWaveType() )
            {
            case UICC::Wave_t::SINUS:
                Wave = std::make_shared<SinusC>();
                break;
            case UICC::Wave_t::TRIANGLE:
                Wave = std::make_shared<TriangleC>();
                break;
            case UICC::Wave_t::RECTANGLE:
                Wave = std::make_shared<RectangleC>();
                break;
            }
            Wave->SetWaveParameters( Interface.GetAmplitudeParameter(), Interface.GetPeriodsParameter(), Interface.GetLengthParameter() );
            Wave->GenerateWave();

            if( Interface.IsFileRequired() )
            {
                Wave->SaveWaveToFile( Interface.GetFileName() );
            }
            else
            {
                Wave->DisplayWave();
            }
        }
        else
            std::cout << "ERROR:  One or more parameters are missing or not correct!" << std::endl;
    }
    catch( std::exception& e )
    {
        std::cout << e.what() << std::endl;
    }

    return EXIT_SUCCESS;
}
