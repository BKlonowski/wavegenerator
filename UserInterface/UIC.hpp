#pragma once

#include <iostream>
#include <string>

#include <map>



//================================================================================================================
// Represents the user interface which handles the program commands and messages
//================================================================================================================
class UICC
{
public:
    //=================================================================================================
    // Overloaded constructor of the wave
    // argc - contains the number of arguments given to the application
    // argv - contains the set of arguments 
    //=================================================================================================
    UICC( const int& argc, char** argv );

    //=================================================================================================
    // Checks whether the help indicator is contained within the application parameter
    //=================================================================================================
    const bool IsHelpNeeded( void );

    //=================================================================================================
    // Gives the status of the file requirement - true if saving to the file was requested by the user
    //=================================================================================================
    const bool IsFileRequired( void );

    //=================================================================================================
    // Handles the help requested by the user
    //=================================================================================================
    void HandleHelpParameter( void );

    //=================================================================================================
    // Gives indication of the parameters - whether they were all correctly specified by the user
    //=================================================================================================
    const bool AreParametersAvailableAndCorrect( void );

    //=================================================================================================
    // Getters for the parameters of the wave
    //=================================================================================================
    const float GetAmplitudeParameter( void ) const;
    const float GetPeriodsParameter( void ) const;
    const float GetLengthParameter( void ) const;

    const std::string& GetFileName( void ) const;

    // Contains the types of wave that can be chosen by the user
    typedef enum
    {
        SINUS,
        TRIANGLE,
        RECTANGLE,
    } Wave_t;

    const Wave_t& GetWaveType( void ) const;

private:

    //=================================================================================================
    // Checks whether the numerical parameter given as c-string is correct - does it contain only digits
    //=================================================================================================
    bool IsParameterCorrect( const char* parameterAsString );

    //=================================================================================================
    // Checks whether the parameter indicator is correct and is not duplicated
    //=================================================================================================
    bool IsParameterCorrect( const char parameterIndicator );

    //=================================================================================================
    // Checks what kind of wave is chosen by the user and saves it in the class field
    //=================================================================================================
    void ParseWaveType( const std::string& waveNameArgument );

    // Holds the wave parameter indicators :
    // a  -  amplitude
    // p  -  periods
    // l  -  length
    constexpr static const char WAVE_INPUT_PARAMETERS[] = { 'a', 'p', 'l' };
    
    // Additional and optional arguments
    constexpr static const char HELP_INDICATOR = 'h';
    constexpr static const char FILE_INDICATOR = 'f';
    constexpr static const char WAVE_INDICATOR = 'w';

    // Keeps the current status of the file requirement and its name as string
    bool isFileRequired;
    std::string waveDataFileName;

    Wave_t chosenWave;

    // Keeps the information about the help required to be displayed
    bool isHelpRequired;
    
    // NOTE: it is filled with initial parameters (ther indicators and values) as zeros
    std::map<char, float> arguments;

};
