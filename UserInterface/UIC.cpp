#include "UIC.hpp"
#include <fstream>
#include <stdexcept>


//=================================================================================================
void PrintFileTextContext( std::ifstream& fileToRead )
{
    while( true == fileToRead.good() && false == fileToRead.eof() )
    {
        std::cout << static_cast<char>(fileToRead.get());
    }
    std::cout << std::endl;
}
//=================================================================================================



//=================================================================================================
UICC::UICC( const int& argc, char** argv ) : isHelpRequired(false), isFileRequired(false)
{
    for( int currentArgument = 1; currentArgument < argc; ++currentArgument )
    {
        // Search for the first '-' sign
        if( argv[ currentArgument ][ 0 ] == '-' )
        {
            // Check whether the help needs to be displayed or continue to parse the parameters
            if( argv[ currentArgument ][ 1 ] == HELP_INDICATOR )
            {
                isHelpRequired = true;
            }
            else if( argv[ currentArgument ][ 1 ] == FILE_INDICATOR )
            {
                isFileRequired = true;

                // Save the name of the file as string
                if( argv[ currentArgument + 1 ] != nullptr )
                    waveDataFileName.append( argv[ ++currentArgument ] );
                else
                    throw std::exception( "ERROR:  File name malformed or missing!" );
            }
            else if( argv[ currentArgument ][ 1 ] == WAVE_INDICATOR )
            {
                // Save the name of the wave as string
                if( argv[ currentArgument + 1 ] != nullptr )
                    ParseWaveType( argv[ ++currentArgument ] );
            }
            else if( IsParameterCorrect( argv[ currentArgument + 1 ] ) && IsParameterCorrect( argv[ currentArgument ][1] ) )
            {
                arguments.insert( std::make_pair( argv[ currentArgument ][ 1 ], static_cast<float>(::atof( argv[ currentArgument + 1 ] ) ) ) );
                ++currentArgument;
            }
            else
                throw std::exception( "ERROR:  Not a proper parameter!" );
        }
        else
            throw std::exception( "ERROR:  Not a proper parameter indicator!" );
    }
}
//=================================================================================================


//=================================================================================================
void UICC::HandleHelpParameter( void )
{
    std::ifstream helpTextFile;
    helpTextFile.open( "../UserInterface/wghlp", std::ios::in );
    
    if( true == helpTextFile.is_open() )
    {
        PrintFileTextContext( helpTextFile );
    }
    else
    {
        std::cout << "ERROR:  Help context not found!" << std::endl;
    }
}
//=================================================================================================


//=================================================================================================
bool UICC::IsParameterCorrect( const char* parameterAsString )
{
    if( nullptr != parameterAsString )
    {
        for( unsigned int characterNumber = 0; characterNumber < strlen( parameterAsString ); ++characterNumber )
        {
            const char& characterToCheck = (parameterAsString[ characterNumber ]);
            if( '.' != characterToCheck && ('0' > characterToCheck  || '9' < characterToCheck) )
            {
                return false;
            }
        }

        // If the function got here the parameter contains only the proper digits
        return true;
    }
    else
    {
        std::cout << "ERROR:  Parameter given in bad format!" << std::endl;
        return false;
    }
}
//=================================================================================================


//=================================================================================================
bool UICC::IsParameterCorrect( const char parameterAsString )
{
    // Check whether given parameter match one of the available params
    for( unsigned int checkedParamIndex = 0; checkedParamIndex < strlen( WAVE_INPUT_PARAMETERS ); ++checkedParamIndex )
    {
        if( parameterAsString == WAVE_INPUT_PARAMETERS[ checkedParamIndex ] )
        {
            // check for duplicates in the specified parameters
            for( auto it = arguments.begin(); it != arguments.end(); ++it )
            {
                if( it->first == parameterAsString )
                    return false;
            }

            // No duplicate found and value matches
            return true;
        }
    }

    // If the function gets here then no matching parameter indicator was found and given parameter is not correct
    return false;
}
//=================================================================================================


//=================================================================================================
const bool UICC::IsHelpNeeded( void )
{
    return isHelpRequired;
}
//=================================================================================================


//=================================================================================================
const bool UICC::IsFileRequired( void )
{
    return isFileRequired;
}
//=================================================================================================


//=================================================================================================
const bool UICC::AreParametersAvailableAndCorrect( void )
{
    if( 3 == arguments.size() )
    {
        for( auto it = arguments.begin(); it != arguments.end(); ++it )
        {
            if( it->second <= 0 )
                return false;
        }

        return true;
    }

    // Not enough parameters available in the map
    return false;
}
//=================================================================================================


//=================================================================================================
const float UICC::GetAmplitudeParameter( void ) const
{
    return arguments.at( 'a' );
}
//=================================================================================================


//=================================================================================================
const float UICC::GetPeriodsParameter( void ) const
{
    return arguments.at( 'p' );
}
//=================================================================================================


//=================================================================================================
const float UICC::GetLengthParameter( void ) const
{
    return arguments.at( 'l' );
}
//=================================================================================================


//=================================================================================================
const std::string& UICC::GetFileName( void ) const
{
    if( !!waveDataFileName.empty() )
        throw std::exception( "ERROR:  File name malformed or missing!" );
    return waveDataFileName;
}
//=================================================================================================


//=================================================================================================
const UICC::Wave_t& UICC::GetWaveType( void ) const
{
    return chosenWave;
}
//=================================================================================================


//=================================================================================================
void UICC::ParseWaveType( const std::string& waveNameArgument )
{
    if( waveNameArgument == "sinus" )
        chosenWave = SINUS;
    else if( waveNameArgument == "triangle" )
        chosenWave = TRIANGLE;
    else if( waveNameArgument == "rectangle" )
        chosenWave = RECTANGLE;
    else
        throw std::exception( "ERROR:  Wrong wave name given!" );
}
//=================================================================================================
