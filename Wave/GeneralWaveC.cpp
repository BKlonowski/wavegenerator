#include "GeneralWaveC.hpp"
#include <string>

//=================================================================================================
WaveC::WaveC( void )
{
}
//=================================================================================================


//=================================================================================================
void WaveC::FillWaveWithSamples( float( *SampleGenerator )(unsigned int rawSample, float periods, float length) )
{
    std::cout << "|   Wave generating status: " << std::endl;

    for( unsigned int period = 0; period < periods; ++period )
    {
        for( unsigned int sampleNumber = 0; sampleNumber < length; ++sampleNumber )
        {
            // These instructions diplay the status of the wave generation.
            // One sample is being added to the count so the user can see the full amount of samples
            std::cout << "\r";
            std::cout << "|   " << length*period + sampleNumber + 1 << " / " << length*periods;
            wave.push_back( amplitude * SampleGenerator(sampleNumber, static_cast<float>(period), length) );
        }
    }
}
//=================================================================================================


//=================================================================================================
void WaveC::SetWaveParameters( float amplitude, float periods, float length )
{
    if( periods > 0 && length > 0 && amplitude > 0 )
    {
        this->amplitude = amplitude;
        this->length = length;
        this->periods = periods;
    }
    else
    {
        std::cout << "ERROR:   Wrong parameters specified!" << std::endl;
    }
}
//=================================================================================================


//=================================================================================================
void WaveC::DisplayWave( void )
{
    for( unsigned int sampleIndex = 0; sampleIndex < wave.size(); ++sampleIndex )
    {
        std::cout << sampleIndex << "\t" << wave.at( sampleIndex ) << std::endl;
    }
}
//=================================================================================================


//=================================================================================================
void WaveC::SaveWaveToFile( std::string fileName )
{
    if( !fileName.empty() )
    {
        std::ofstream waveFile;

        waveFile.open( fileName.c_str() );
        if( waveFile.is_open() )
        {
            std::cout << "|   Saving the wave to the file: " << fileName << std::endl;
            for( unsigned int sampleIndex = 0; sampleIndex < wave.size(); ++sampleIndex )
            {
                std::string newLine = std::to_string(sampleIndex) + "\t" + std::to_string(wave.at( sampleIndex ));
                waveFile.write( newLine.c_str(), newLine.size() );
                waveFile.put( '\n' );
            }
        }

        waveFile.close();
    }
}
//=================================================================================================
