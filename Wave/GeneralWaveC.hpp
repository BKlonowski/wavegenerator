#pragma once

#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>


//================================================================================================================
// Represents the sinus wave which can be generated
//================================================================================================================
class WaveC
{
public:
    //=================================================================================================
    // Constructor of the wave in general
    //=================================================================================================
    WaveC( void );

    //=================================================================================================
    // Generates the requested wave with selected parameters
    // SampleGenerator is the function called to get the next sample of the wave
    //=================================================================================================
    void FillWaveWithSamples( float( *SampleGenerator )(unsigned int rawSample, float periods, float length) );


    virtual void GenerateWave( void ) = 0;


    void SetWaveParameters( float amplitude, float period, float length );

    //=================================================================================================
    // Displays the wave directly in the terminal (depends on the current execution file launching)
    //=================================================================================================
    void DisplayWave( void );

    //=================================================================================================
    // Saves the generated wave in the specified file
    // fileName - stores the name of the file (should be taken from the interface getter)
    //=================================================================================================
    void SaveWaveToFile( std::string fileName );


private:
    // The generated wave is represented as the vector of samples - can be displayed directly or saved to file
    std::vector<float> wave;

    // Parameters of the requested wave - can be specified by setters, or by overloaded constructor
    float amplitude;
    float periods;
    float length;
};
