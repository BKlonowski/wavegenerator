#pragma once

#include "../Wave/GeneralWaveC.hpp"

//================================================================================================================
// Represents the sinus wave which can be generated
//================================================================================================================
class TriangleC : public WaveC
{
public:
    //=================================================================================================
    // Default constructor of the sinus class
    //=================================================================================================
    TriangleC();

    //=================================================================================================
    // Default destructor of the sinus class
    //=================================================================================================
    ~TriangleC();

    //=================================================================================================
    // Generates the desired sinus by using the standard  sin()  function to generate each sample
    //=================================================================================================
    void GenerateWave( void ) override;


private:

    // The value of PI being used to convert the radians to degrees
    constexpr static const float PI = 3.14159265f;
};
